// ==UserScript==
// @name         Juick comments
// @version      0.3
// @match        https://juick.com/*
// @grant        none
// @require https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @downloadURL https://gitlab.com/alkadiene/juick-comments/raw/master/juick-comments.user.js
// @updateURL   https://gitlab.com/alkadiene/juick-comments/raw/master/juick-comments.user.js
// ==/UserScript==

var commentsTree = [];

(function() {
    'use strict';

    $.each($('ul#replies').children('li').find('div.msg-links'), function(i, e) {
        var el = $(e).parent();
        var text = $(e).text();
        var pos = text.substr(1).indexOf('/');
        var id = +el.parent()[0].id; // вместо i, потому что камменты могут быть удалены
        if (commentsTree[id] == undefined) {
            commentsTree[id] = [];
        }
        if (pos > 0) {
            var parentnumstr = text.substr(pos+2);
            parentnumstr = parentnumstr.substr(0, parentnumstr.indexOf(' '));
            var parentnum = +parentnumstr;

            if (commentsTree[parentnum] == undefined) {
                commentsTree[parentnum] = [];
            }
            commentsTree[parentnum].push(id);
        }
    });

    //    console.log(commentsTree);

    $.each($('ul#replies').children('li').find('div.msg-links'), function(i, e) {
        var el = $(e).parent();
        var id = el.parent()[0].id; // вместо i, потому что камменты могут быть удалены
        moveReplies(id);
    });

})();

function moveReplies(commentNum) {
    var parentComment = $('li#' + commentNum);
    $.each(commentsTree[commentNum], function(ii, childCommentNum) {
        var oldChildComment = $('li#' + childCommentNum);

        var newChildComment = oldChildComment.clone(true);
        parentComment.append(newChildComment);
        newChildComment.css('margin-left', '15px');
        oldChildComment.remove();
        //        C();
        moveReplies(childCommentNum);
    });
}